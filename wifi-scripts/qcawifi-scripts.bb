DESCRIPTION = "WiFi Scripts and Config files"
LICENSE = "ISC"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/${LICENSE};md5=f3b90e78ea0cffb20bf5cca7947a896d"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

inherit update-alternatives systemd
INITSCRIPT_NAME = "qcawifi"

SYSTEMD_SERVICE_${PN} = "qcawifi.service"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"

S = "${WORKDIR}/wifi-scripts"

SRC_URI = "file://qcawifi"

FILES_${PN} += "${baselib}/ath/*"
FILES_${PN} += "/etc/*"

do_install() {
	install -d ${D}/etc/ath/service_scripts/
	install -d ${D}${systemd_unitdir}/system/
	install -m 0755 ${WORKDIR}/qcawifi/etc/ath/service_scripts/rc.wlan ${D}/etc/ath/service_scripts/rc.wlan
	install -m 0755 ${WORKDIR}/qcawifi/etc/ath/service_scripts/read_caldata_to_fs.sh ${D}/etc/ath/service_scripts/read_caldata_to_fs.sh
	install -m 0755 ${WORKDIR}/qcawifi/etc/ath/wifi_module_params ${D}/etc/ath/wifi_module_params

	install -m 0755 ${WORKDIR}/qcawifi/etc/ath/service_scripts/update_smp_affinity.sh ${D}/etc/ath/service_scripts/update_smp_affinity.sh
	install -m 0644 ${WORKDIR}/qcawifi/qcawifi.service ${D}${systemd_unitdir}/system/
}
