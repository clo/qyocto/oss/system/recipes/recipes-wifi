DESCRIPTION = "Wpa_Supplicant Scripts and Config files"
LICENSE = "ISC"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/${LICENSE};md5=f3b90e78ea0cffb20bf5cca7947a896d"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

inherit update-alternatives systemd
INITSCRIPT_PACKAGES = "wapsupplicant-scripts"
INITSCRIPT_NAME = "qca-hostapd"

S = "${WORKDIR}/wifi-scripts"

SRC_URI = "file://wpasupplicant/wpa-supplicant.service"

SYSTEMD_SERVICE_${PN} = "wpa-supplicant.service"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"

do_install_append() {
	install -d ${D}${systemd_unitdir}/system/
	install -D -m 0644 ${WORKDIR}/wpasupplicant/wpa-supplicant.service ${D}${systemd_unitdir}/system/
}

FILES_${PN} = " \
	${systemd_unitdir}/system/wpa-supplicant.service \
	"

