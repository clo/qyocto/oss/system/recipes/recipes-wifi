DESCRIPTION = "Hostap Scripts and Config files"
LICENSE = "ISC"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/${LICENSE};md5=f3b90e78ea0cffb20bf5cca7947a896d"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

inherit update-alternatives systemd
INITSCRIPT_PACKAGES = "hostapd-scripts"
INITSCRIPT_NAME = "qca-hostapd"

S = "${WORKDIR}/wifi-scripts"

SRC_URI = "file://hostapd"

SYSTEMD_SERVICE_${PN} = "hostapd.service"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"

do_install() {
	install -d ${D}${systemd_unitdir}/system/

        install -m 0644 ${WORKDIR}/hostapd/hostapd.service ${D}${systemd_unitdir}/system/
}

